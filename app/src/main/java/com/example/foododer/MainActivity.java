package com.example.foododer;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button pesanEmail;
    private Button pesanTelp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pesanEmail = (Button) findViewById(R.id.btn_pesanEmail);
        pesanEmail.setOnClickListener(this);

        pesanTelp = (Button) findViewById(R.id.btn_pesanTelp);
        pesanTelp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_pesanEmail:
                Intent moveIntent = new Intent(MainActivity.this, SubmainPemesananActivity.class);
                startActivity(moveIntent);
                break;
            case R.id.btn_pesanTelp:
                String phoneNumber = "085742169383";
                Intent dialPhoneIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+phoneNumber));
                startActivity(dialPhoneIntent);
                break;
        }
    }
}
